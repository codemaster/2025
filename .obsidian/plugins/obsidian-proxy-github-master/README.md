

# Obsidian 代理插件/Github代理

因为某些原因，在国内经常无法下载 Obsidian 的社区插件。这个项目的主要目的就是修复这种情况，让国内的用户也可以无障碍的下载社区插件。


## Changelog & Todo
- [x] 去除无法使用的镜像站
- [x] 增加新的可用镜像站 验证时间 2023.10.13
- [x] 增加自动关联镜像列表(之前是手动在代码里一个一个添加)


### 上手指南

1. 下载 [obsidian-proxy-github.zip](https://gitee.com/juqkai/obsidian-proxy-github/releases)
2. 解压 obsidian-proxy-github.zip
3. 将解压的文件夹放入笔记目录下的插件目录内。如：XXX/.obsidian/plugins


### 版权说明

该项目签署了 Apache License 2.0 授权许可

### 鸣谢

- https://github.com/juqkai/obsidian-proxy-github
